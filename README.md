# Jenkins Install

**Start the my-jenkins container**

`docker-compose up -d`

**Get the initial admin password**

`docker exec my-jenkins cat /var/jenkins_home/secrets/initialAdminPassword`

**Confirm the my-jenkins container is running**

`docker ps`
